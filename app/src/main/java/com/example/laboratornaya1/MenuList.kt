package com.example.laboratornaya1

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_list.*
import kotlinx.android.synthetic.main.activity_movie.*

class MenuList : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        textView1.setOnClickListener {
            var intent = Intent(this, MoviesFiltered::class.java);
            intent.putExtra("movies", getMoviesList(textView1.text.toString()));
            startActivity(intent);
        }

        textView2.setOnClickListener {
            var intent = Intent(this, MoviesFiltered::class.java);
            intent.putExtra("movies", getMoviesList(textView2.text.toString()));
            startActivity(intent);
        }

    }
}