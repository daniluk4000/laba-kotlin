package com.example.laboratornaya1

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_home.*

abstract class BaseActivity : AppCompatActivity() {

    public fun getMoviesList(search: String): ArrayList<Movies> {
        val movies_list = arrayListOf<Movies>()

        movies_list.add(
            Movies(
                "Мэрри Поппинс возвращается",
                "США",
                6.6,
                "Фэнтези",
                "Фильм рассказывает о новых приключениях Мэри и ее друга Джека, которым предстоит встретиться с представителями следующего поколения семейства Бэнкс."
            )
        )

        movies_list.add(
            Movies(
                "Путь домой",
                "США",
                7.1,
                "Мелодрама",
                "Что делать, если тебя и твоего любимого хозяина разделяют сотни километров? Белла знает ответ: она обязательно вернется домой, чего бы ей это ни стоило. И путь ее будет полон невероятных приключений, опасностей и удивительных открытий."
            )
        )

        movies_list.add(
            Movies(
                "Бамблби",
                "США",
                6.6,
                "Фэнтези",
                "1987 год. Скрываясь от преследования, Бамблби прибывает на Землю, где тут же сталкивается с военными. В результате стычки с землянами и десептиконом, Бамблби лишается голосового модуля, повреждает память и перед отключкой сканирует стоящий неподалёку Фольксваген Жук. Через некоторое время девушка по имени Чарли находит его на автомобильной свалке в калифорнийском городке и получает в качестве подарка на 18-летие. Закатив машину в свой гараж, Чарли с удивлением обнаруживает, что ей достался не простой жёлтый Фольксваген Жук."
            )
        )

        if (!search.equals("", true)) {
            val movies_filter = movies_list.filter { x -> x.filmCategory.equals(search, true) } as ArrayList
            return movies_filter
        }

        return movies_list;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater
        inflater.inflate(R.menu.header_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        pref = getSharedPreferences("project", Context.MODE_PRIVATE)
        when (item.itemId) {
            R.id.menu_about -> {
                val intent = Intent(this@BaseActivity, About::class.java)
                startActivity(intent)
                return true
            }
            R.id.menu_list -> {
                val intent = Intent(this@BaseActivity, MenuList::class.java)
                intent.putExtra("movies", getMoviesList(""))
                startActivity(intent)
                return true
            }
            R.id.menu_exit -> {
                val editor = pref.edit()
                editor.clear();
                editor.apply();
                val intent = Intent(this@BaseActivity, MainActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
