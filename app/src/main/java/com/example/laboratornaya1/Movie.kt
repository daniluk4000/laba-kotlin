package com.example.laboratornaya1

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_movie.*

class Movie : Home() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)

        var movie = intent.getSerializableExtra("movie") as Movies

        name.text = movie.filmName
        country.text = movie.filmCountry
        rating.text = String.format(
            "Рейтинг: %1s",
            movie.filmRating.toString()
        )
        description.text = movie.filmDescription

    }
}