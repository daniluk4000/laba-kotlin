package com.example.laboratornaya1

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Movie
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Movies(
    @SerializedName("filmName") var filmName: String = "",
    @SerializedName("filmCountry") var filmCountry: String = "",
    @SerializedName("filmRating") var filmRating: Number = 0,
    @SerializedName("filmCategory") var filmCategory: String = "",
    @SerializedName("filmDescription") var filmDescription: String = ""
) : Serializable

class MoviesAdapter(private val context: Context, private val dataSource: ArrayList<Movies>) :
    BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater;

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val rowView = inflater.inflate(R.layout.movies_list_item, parent, false)

        val filmNameView = rowView.findViewById(R.id.name) as TextView
        filmNameView.text = getItem(position).filmName

        val filmCountryView = rowView.findViewById(R.id.country) as TextView
        filmCountryView.text = getItem(position).filmCountry

        val filmRatingView = rowView.findViewById(R.id.rating) as TextView
        filmRatingView.text = String.format(
            "Рейтинг: %1s",
            getItem(position).filmRating.toString()
        )

        return rowView
    }

    override fun getItem(position: Int): Movies {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataSource.size
    }

}