package com.example.laboratornaya1

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_home.*

open class Home : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        moviesList.adapter = MoviesAdapter(this, getMoviesList(""))
        moviesList.setOnItemClickListener { _, _, position, _ ->
            var movie = getMoviesList("")[position]
            var intent = Intent(this, Movie::class.java);
            intent.putExtra("movie", movie);
            startActivity(intent);
        }
    }
}
