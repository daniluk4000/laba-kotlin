package com.example.laboratornaya1

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

lateinit var pref: SharedPreferences

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        pref = getSharedPreferences("project", Context.MODE_PRIVATE)
        if (pref.contains("LOGGED_IN")) {
            val intent = Intent(this, Home::class.java)
            startActivity(intent);
            finish()
        } else {
            submit.setOnClickListener {
                if (Password.text.toString().equals("password", false)) {
                    val editor = pref.edit()
                    editor.putInt("LOGGED_IN", 1)
                    editor.apply()
                    val intent = Intent(this@MainActivity, Home::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    Error.text = "Ошибка: неверный пароль"
                }
            }
        }
    }
}
