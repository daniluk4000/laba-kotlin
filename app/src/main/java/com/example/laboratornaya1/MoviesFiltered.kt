package com.example.laboratornaya1

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_filtered_list.*

class MoviesFiltered : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filtered_list)
        var movies: ArrayList<Movies> = intent.getSerializableExtra("movies") as ArrayList<Movies>
        moviesList.adapter = MoviesAdapter(this, movies)
        moviesList.setOnItemClickListener { _, _, position, _ ->
            var movie = movies[position]
            var intent = Intent(this, Movie::class.java);
            intent.putExtra("movie", movie);
            startActivity(intent);
        }
    }
}